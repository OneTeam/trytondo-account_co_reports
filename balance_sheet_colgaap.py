# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.transaction import Transaction
from trytond.report import Report
from trytond.pyson import Eval, PYSONEncoder
from trytond.pool import Pool
import operator


def compute_report(data, domain, codes, statement='income'):
    pool = Pool()
    Fiscalyear = pool.get('account.fiscalyear')
    Period = pool.get('account.period')
    Account = pool.get('account.account')

    period = None
    amount_profit = 0
    res = {}

    ctx = {
        'posted': data['posted'],
    }

    if data.get('period'):
        period = Period(data['period'])
        periods = Period.search([
            ('start_date', '<=', period.start_date),
        ])
        periods_ids = [period.id]
        periods_ids.extend([p.id for p in periods])
        ctx['periods'] = periods_ids
    elif data.get('periods'):
        ctx['periods'] = data.get('periods')
    else:
        ctx['fiscalyear'] = data['fiscalyear']

    if data.get('detailed'):
        len_code = 6
    else:
        len_code = 5
        domain.append(
            ('type', '=', ''),
        )

    view_accounts = Account.search(domain)

    reduce_ids = [a.id for a in view_accounts
                  if len(a.code) <= len_code]

    with Transaction().set_context(ctx):
        accounts = Account.search([
            ('id', 'in', reduce_ids),
        ], order=[('code', 'ASC')]
        )
        if data.get('account_profit'):
            account_profit = Account(data['account_profit'])
            profit_accounts = Account.search([
                'OR',
                ('code', 'in', ['4', '5', '6', '7']),
            ])
            amount_profit = sum([a.balance for a in profit_accounts])

        records = []
        for a in accounts:
            if (len(a.code) == 1) or not (
                    a.balance == 0 and len(a.code) >= (len_code - 1)):
                if statement == 'income':
                    a.balance = a.balance * (-1)
                records.append(a)
        res['records'] = records
        main_accounts = Account.search(['OR',
                                        ('code', 'in', codes),
                                        ])

    # Crea una cuenta virtual temporal con la utilidad
    # para ponerla en el Balance
    if data.get('account_profit'):

        tree_account_assets = []

        def _add_parents(acc):
            if acc in res['records']:
                acc = res['records'].pop(res['records'].index(acc))
            acc.balance = acc.balance + amount_profit
            tree_account_assets.append(acc)
            if acc.parent and acc.parent.code:
                _add_parents(acc.parent)

        # Split records for keep order
        _add_parents(account_profit)
        res['records'].extend(tree_account_assets)
        new_records = [(rec.code, rec) for rec in res['records']]

        def getKey(item):
            return item[1]

        new_records.sort(key=operator.itemgetter(0))
        res['records'] = [nr[1] for nr in new_records]

    global_result = sum([a.balance for a in main_accounts]) + amount_profit
    if statement == 'income':
        global_result = global_result * (-1)

    res['global_result'] = global_result
    fiscalyear = Fiscalyear(data['fiscalyear'])
    if period:
        res['start_date'] = period.start_date
        res['end_date'] = period.end_date
    else:
        periods_dates = []
        for p in fiscalyear.periods:
            periods_dates.extend([p.start_date, p.end_date])
        res['start_date'] = min(periods_dates)
        res['end_date'] = max(periods_dates)

    res['period'] = period
    res['fiscalyear'] = fiscalyear
    return res


class PrintBalanceSheetCOLGAAPStart(ModelView):
    'Print Balance Sheet COLGAAP Start'
    __name__ = 'account_co_reports.print_balance_sheet_colgaap.start'
    fiscalyear = fields.Many2One(
        'account.fiscalyear', 'Fiscal Year',
        help='Leave empty for all open fiscal year', required=True)
    posted = fields.Boolean('Posted Moves', help='Show posted moves only')
    period = fields.Many2One('account.period', 'Period', domain=[
        ('fiscalyear', '=', Eval('fiscalyear')),
    ], depends=['fiscalyear'])
    company = fields.Many2One('company.company', 'Company', required=True)
    detailed = fields.Boolean('Detailed')
    account_profit = fields.Many2One('account.account',
                                     'Account Profit', domain=[
                                         ('type', '!=', ''),
                                         ('code', 'like', '36%'),
                                     ])

    @staticmethod
    def default_posted():
        return False

    @staticmethod
    def default_company():
        return Transaction().context.get('company')


class PrintBalanceSheetCOLGAAP(Wizard):
    'Print Balance Sheet COLGAAP'
    __name__ = 'account_co_reports.print_balance_sheet_colgaap'
    start = StateView(
        'account_co_reports.print_balance_sheet_colgaap.start',
        'account_co_reports.print_balance_sheet_colgaap_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('account_co_reports.balance_sheet_colgaap')

    def do_print_(self, action):
        Period = Pool().get('account.period')
        account_profit_id = None

        ctx = {
            'fiscalyear': (self.start.fiscalyear.id
                           if self.start.fiscalyear else None),
            'posted': self.start.posted,
            'cumulate': True,
        }
        if self.start.account_profit:
            account_profit_id = self.start.account_profit.id
        if self.start.period:
            periods = Period.search([
                ('end_date', '<', self.start.period.start_date),
            ])
            periods_ids = [self.start.period.id]
            periods_ids.extend([p.id for p in periods])
            ctx['periods'] = periods_ids

        action['pyson_context'] = PYSONEncoder().encode(ctx)

        period_id = None
        if self.start.period:
            period_id = self.start.period.id
        data = {
            'fiscalyear': self.start.fiscalyear.id,
            'period': period_id,
            'company': self.start.company.id,
            'detailed': self.start.detailed,
            'posted': self.start.posted,
            'account_profit': account_profit_id,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class BalanceSheetCOLGAAP(Report):
    __name__ = 'account_co_reports.balance_sheet_colgaap'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(
            BalanceSheetCOLGAAP, cls).get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        company = Company(data['company'])

        codes = ['2', '3']
        domain = [
            ('code', '<', '4'),
        ]
        res = compute_report(data, domain, codes, statement='balance')

        report_context.update(res)
        report_context['detailed'] = data['detailed']
        report_context['company'] = company
        report_context['fiscalyear2'] = None
        return report_context
