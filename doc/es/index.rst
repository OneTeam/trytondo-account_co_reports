================================
Reportes Contables para Colombia
================================

Basado en: `trytonpsk-account_col: <https://bitbucket.org/presik/trytonpsk-account_col>`_

Diferencias con la versión original:

1. Se actualiza a la versón 5.4 y superiores
2. Se cambian los archivos de extensión .ods a .fods
3. Cada informe queda separado en un archivo .py y .fodt, con la intención de integrarlos posteriormente en categorías de informes
4. No necesita ninguna dependencia por fuera de los módulos oficiales
5. No hay cambios en los módulos oficiales como party, account u otro
6. Se agrega reporte Party Book Account, el cual no incluye impresión
7. Se hacen parches y cambios menores a los archivos .ods convertidos en .fods
8. No se incluyen los informes certificado de retención, impuestos por factura e impuestos contabilizados, no están incluidos

Aclaraciones:

1. Los informes Balance General y Perdidas y Ganancias, depende de la estructura del PUC para Colombia en el módulo account_co_pymes, como se describen a continuación:
   1 => Activo
   2 => Pasivo
   3 => Patrimonio
   4 => Ingresos
   5 => Gastos
   6 y 7 => Costos




