===============================
Accounting Reports for Colombia
===============================
                                                                                
Based on: `trytonpsk-account_col: <https://bitbucket.org/presik/trytonpsk-account_col>`_

Differences to the original version:

1. It is updated to version 5.4 and higher
2. The .ods extension files are changed to .fods
3. Each report is separated into a .py and .fodt file, with the intention of later integrating them into report categories
4. You do not need any dependency outside the official modules
5. There are no changes in the official modules such as party, account or other
6. Party Book Account report is added, which does not include printing
7. Patches and small changes are made to .ods files converted to .fods
8. The party_withholding, taxes_by_invoice y taxes_posted, are not included.

Clarifications:

1. The income_statement_colgaap and balance_sheet_colgaap reports depend on the structure of the PUC for Colombia in the account_co module,
   as described below:
   1 => Asset
   2 => Liability
   3 => Equity
   4 => Income
   5 => Expenses
   6 and 7 => Costs
