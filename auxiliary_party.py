# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval


__all__ = ['AuxiliaryPartyStart', 'PrintAuxiliaryParty', 'AuxiliaryParty']


class AuxiliaryPartyStart(ModelView):
    'Auxiliary Party Start'
    __name__ = 'account_co_reports.print_auxiliary_party.start'

    start_period = fields.Many2One(
        'account.period', 'Start Period',
        domain=[
            ('start_date', '<=', (Eval('end_period'), 'start_date')),
        ], depends=['end_period'])
    end_period = fields.Many2One(
        'account.period', 'End Period',
        domain=[
            ('start_date', '>=', (Eval('start_period'), 'start_date'))
        ],
        depends=['start_period'])
    party = fields.Many2One('party.party', 'Party')
    accounts = fields.Many2Many('account.account', None, None, 'Accounts',
                                domain=[
                                    ('type', '!=', ''),
                                ])
    company = fields.Many2One('company.company', 'Company', required=True)
    posted = fields.Boolean('Posted Move', help='Show only posted move')
    grouped_by_account = fields.Boolean('Grouped by Account')
    empty_account = fields.Boolean('Empty Account',
                                   help='With account without move')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_posted():
        return False

    @staticmethod
    def default_empty_account():
        return False


class PrintAuxiliaryParty(Wizard):
    'Print Auxiliary Party'
    __name__ = 'account_co_reports.print_auxiliary_party'

    start = StateView(
        'account_co_reports.print_auxiliary_party.start',
        'account_co_reports.print_auxiliary_party_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-print', default=True),
        ])
    print_ = StateReport('account_co_reports.auxiliary_party')

    def do_print_(self, action):
        if self.start.start_period:
            start_period = self.start.start_period.id
        else:
            start_period = None
        if self.start.end_period:
            end_period = self.start.end_period.id
        else:
            end_period = None
        if not self.start.party:
            party = None
        else:
            party = self.start.party.id

        if self.start.accounts:
            accounts_ids = [acc.id for acc in self.start.accounts]
        else:
            accounts_ids = []
        data = {
            'company': self.start.company.id,
            'start_period': start_period,
            'end_period': end_period,
            'posted': self.start.posted,
            'party': party,
            'empty_account': self.start.empty_account,
            'accounts': accounts_ids,
            'grouped_by_account': self.start.grouped_by_account,
        }
        return action, data

    def transition_print_(self):
        return 'end'


class AuxiliaryParty(Report):
    __name__ = 'account_co_reports.auxiliary_party'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(
            AuxiliaryParty, cls).get_context(records, header, data)
        pool = Pool()
        Period = pool.get('account.period')
        Company = pool.get('company.company')
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')

        company = Company(data['company'])
        dom_move = []
        # Add context Transaction for company and fiscalyear
        #    dom_move = [('company', '=', company)]
        if data.get('posted'):
            dom_move.append(('state', '=', 'posted'))

        start_period = None
        end_period = None
        if data.get('start_period'):
            start_period = Period(data['start_period'])
            dom_move.append(
                ('period.start_date', '>=', start_period.start_date))
        if data.get('end_period'):
            end_period = Period(data['end_period'])
            dom_move.append(('period.start_date', '<=', end_period.start_date))

        moves = Move.search_read(dom_move,
                                 order=[
                                     ('date', 'ASC'), ('id', 'ASC')
                                 ], fields_names=['id'],
                                 )
        moves_ids = [move['id'] for move in moves]

        dom_lines = [
            ('move', 'in', moves_ids)
        ]

        if data.get('accounts'):
            accounts_dom = ('account', 'in', data['accounts'])
            dom_lines.append(accounts_dom)

        if data.get('party'):
            parties_dom = ('party', '=', data['party'])
            dom_lines.append(parties_dom)

        lines = MoveLine.search(dom_lines, order=[('move.date', 'ASC')])

        res = {}

        if lines:
            for line in lines:

                if not line.party:
                    continue

                id_ = line.party.id
                name = line.party.rec_name
                try:
                    # TODO se toma el primer identificador del party
                    # este es totalmente incorrecto
                    if line.party.identifiers:
                        id_number = line.party.identifiers[0].code
                    else:
                        id_number = ''
                except IndexError:
                    pass

                # agrupar por unico identificador party/reference
                if id_ not in res.keys():
                    res[id_] = {
                        'name': name,
                        'id_number': id_number,
                        'accounts': {},
                    }

                # inicializar saldos sin valores de registro
                if line.account not in res[id_]['accounts'].keys():
                    res[id_]['accounts'][line.account] = {
                        'lines': [],
                        'sum_debit': [],
                        'sum_credit': [],
                        'balance': [],
                    }
                res[id_]['accounts'][line.account]['lines'].append(line)
                res[id_]['accounts'][line.account]['sum_debit'].append(
                    line.debit)
                res[id_]['accounts'][line.account]['sum_credit'].append(
                    line.credit)
                res[id_]['accounts'][line.account]['balance'].append(
                    line.debit - line.credit)

        report_context['records'] = res.values()

        report_context['grouped_by_account'] = data['grouped_by_account']
        report_context['start_period'] = \
            start_period.name if start_period else '*'
        report_context['end_period'] = end_period.name if end_period else '*'
        report_context['company'] = company
        return report_context
