# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction


class BalanceSheet(Report):
    'Balance Sheet Report'
    __name__ = 'account.balance_sheet'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super(
            BalanceSheet, cls).get_context(records, header, data)
        Company = Pool().get('company.company')
        report_context['company'] = Company(
            Transaction().context.get('company'))
        report_context['date'] = Transaction().context.get('date')
        report_context['comparison'] = Transaction().context.get('comparison')
        report_context['date_cmp'] = Transaction().context.get('date_cmp')

        value_accounts = report_context['records']
        account_child_list = []
        utility = {}
        value_comp = 0
        for account in value_accounts:
            if account.amount != 0:
                for child in account.childs:
                    if child.name == 'PATRIMONIO NETO Y PASIVOS':
                        child.amount = child.amount + account.amount
                        if child.amount_cmp and account.amount_cmp:
                            child.amount_cmp = \
                                child.amount_cmp + account.amount_cmp
                    for child1 in child.childs:
                        if child1.name == 'PATRIMONIO NETO':
                            child1.amount = child1.amount + account.amount
                            if child1.amount_cmp and account.amount_cmp:
                                child1.amount_cmp = \
                                    child1.amount_cmp + account.amount_cmp
                            child1.childs[0].amount = \
                                child1.childs[0].amount + account.amount
                            if child1.childs[0].amount_cmp and \
                               account.amount_cmp:
                                child1.childs[0].amount_cmp = \
                                    child1.childs[0].amount_cmp + \
                                    account.amount_cmp
                            account_child_list = [
                                child for child in child1.childs[0].childs]
                            if account.amount_cmp:
                                value_comp = account.amount_cmp
                            utility = {
                                'amount': account.amount,
                                'amount_cmp': value_comp,
                                'name':
                                'UTILIDAD / PERDIDA ESTIMADA DEL PERIODO',
                                'childs': None,
                            }
                            account_child_list.append(utility)
                            child1.childs[0].childs = account_child_list
        return report_context
