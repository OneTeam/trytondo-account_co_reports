from trytond.pool import Pool

from . import auxiliary_party
from . import auxiliary_book
from . import party_book_account
from . import trial_balance
from . import trial_balance_detailed
from . import balance_sheet
from . import balance_sheet_colgaap
from . import income_statement
from . import income_statement_colgaap


def register():
    Pool.register(
        auxiliary_party.AuxiliaryPartyStart,
        auxiliary_book.AuxiliaryBookStart,
        trial_balance.PrintTrialBalanceStart,
        party_book_account.PartyBookAccount,
        party_book_account.PartyBookAccountContext,
        trial_balance_detailed.PrintTrialBalanceDetailedStart,
        balance_sheet_colgaap.PrintBalanceSheetCOLGAAPStart,
        income_statement_colgaap.PrintIncomeStatementCOLGAAPStart,
        module='account_co_reports', type_='model')

    Pool.register(
        auxiliary_party.PrintAuxiliaryParty,
        auxiliary_book.PrintAuxiliaryBook,
        trial_balance.PrintTrialBalance,
        trial_balance_detailed.PrintTrialBalanceDetailed,
        balance_sheet_colgaap.PrintBalanceSheetCOLGAAP,
        income_statement_colgaap.PrintIncomeStatementCOLGAAP,
        module='account_co_reports', type_='wizard')

    Pool.register(
        auxiliary_party.AuxiliaryParty,
        auxiliary_book.AuxiliaryBook,
        trial_balance.TrialBalanceClassic,
        balance_sheet.BalanceSheet,
        balance_sheet_colgaap.BalanceSheetCOLGAAP,
        trial_balance_detailed.TrialBalanceDetailed,
        income_statement.IncomeStatement,
        income_statement_colgaap.IncomeStatementCOLGAAP,
        module='account_co_reports', type_='report')
