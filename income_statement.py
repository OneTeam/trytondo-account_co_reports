# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.report import Report
from trytond.pool import Pool
from trytond.transaction import Transaction
from collections import OrderedDict


class IncomeStatement(Report):
    'Income Statement'
    __name__ = 'account.income_statement'

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        Type = pool.get('account.account.type')
        Account = pool.get('account.account')
        Period = pool.get('account.period')
        Fiscalyear = pool.get('account.fiscalyear')
        Company = pool.get('company.company')
        Context = pool.get('account.income_statement.context')
        context = Transaction().context

        report_context = super(IncomeStatement, cls).get_context(
            records, header, data)

        context_fields = Context.fields_get(['start_period', 'fiscalyear'])
        types = Type.search([
            ('statement', '=', 'income')
        ])
        accounts_types = []
        print(context)
        print('---------------------------------------------------')
        print(context_fields)
        print('---------------------------------------------------')
        print(report_context['data'])
        print('---------------------------------------------------')
        records = Type(report_context['data']['id'])
        fiscalyear_id = Transaction().context.get('fiscalyear')
        fiscalyear_cmp = Transaction().context.get('fiscalyear_cmp')
        start_period = Transaction().context.get('start_period')
        start_period_cmp = Transaction().context.get('start_period_cmp')
        end_period = Transaction().context.get('end_period')
        end_period_cmp = Transaction().context.get('end_period_cmp')
        comparison = Transaction().context.get('comparison')
        if start_period:
            start_period = Period(start_period)
        if end_period:
            end_period = Period(end_period)

        dom_periods = [
            ('type', '=', 'standard'),
            ('fiscalyear', '=', fiscalyear_id),
        ]
        if start_period:
            dom_periods.append(('start_date', '>=', start_period.start_date))
        if end_period:
            dom_periods.append(('start_date', '<=', end_period.start_date))
        range_periods = Period.search(dom_periods)
        periods_ids = [p.id for p in range_periods]
        od = {}
        with Transaction().set_context(periods=periods_ids):
            while types:
                type_ = types.pop()
                if type_.statement == 'income':
                    accounts = Account.search([
                        ('type', '=', ''),
                    ])

                    if accounts:
                        setattr(type_, 'accounts', accounts)
                    accounts_types.append((type_.sequence, type_))
                if type_.childs:
                    types.extend(list(type_.childs))

        if accounts_types:
            od = OrderedDict(sorted(dict(accounts_types).items()))

        types_added = []
        filtered_records = []
        for k, v in od.items():
            childs = []
            for c in v.childs:
                if c.id not in types_added:
                    childs.append(c)
                    types_added.extend([v.id, c.id])

            setattr(v, 'childs', childs)
            filtered_records.append(v)

        report_context['start_period'] = start_period
        report_context['start_period_cmp'] = Period(start_period_cmp)
        report_context['end_period'] = end_period
        report_context['end_period_cmp'] = Period(end_period_cmp)
        report_context['fiscalyear'] = Fiscalyear(fiscalyear_id).name
        report_context['fiscalyear_cmp'] = \
            Fiscalyear(fiscalyear_cmp).name if fiscalyear_cmp else ''
        report_context['records'] = filtered_records
        report_context['comparison'] = comparison
        report_context['company'] =\
            Company(Transaction().context.get('company'))
        report_context['date'] = Transaction().context.get('date')

        return report_context
